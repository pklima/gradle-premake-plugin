package com.gitlab.pklima.premake

import com.gitlab.pklima.premake.PremakePlugin
import org.apache.tools.ant.taskdefs.condition.Os
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

public class PremakeTaskTest extends Specification {
    def project = ProjectBuilder.builder().build()
    def task = project.task('premake', type: PremakeTask)

    def setup() {
        task.action = 'vs2019'
    }

    def 'task has some defaults'() {
        expect: 'directory is the default'
            task.directory == project.projectDir

        and: 'just the action is in the arguments'
            task.getArguments() == ['vs2019']

        and: 'premake executable path is the default'
            task.premakeExecutable == new File(PremakePlugin.premakeExecutableName)

        when: 'we change the executable path'
            task.premakeExecutable = new File('test/premake.exe')

        then: 'the path changes'
            task.premakeExecutable == new File('test/premake.exe')

        expect: 'downloaded executable path is the default'
            task.downloadedPremakeExecutable == new File(project.buildDir, PremakePlugin.premakeExecutableName)

        when: 'we change the downloaded executable path'
            task.downloadedPremakeExecutable = 'somewhere/premake5'

        then: 'the path changes'
            task.downloadedPremakeExecutable == new File('somewhere/premake5')
    }

    def 'setting string option'() {
        when: 'add os'
            task.options = [
                os: 'windows'
            ]

        then: '--os is present in arguments'
            task.getArguments() == ['vs2019', '--os=windows']

        when: 'remove os'
            task.options = [
                os: null
            ]

        then: '--os is removed from arguments'
            task.arguments == ['vs2019']
    }

    def 'setting boolean option'() {
        when: 'add verbose'
            task.options = [
                verbose: true
            ]

        then: '--verbose is present in arguments'
            task.arguments == ['vs2019', '--verbose']

        when: 'remove verbose'
            task.options = [
                verbose: false
            ]

        then: '--verbose is removed from arguments'
            task.arguments == ['vs2019']
    }

    def 'setting path option'() {
        when: 'add file as File and scripts as string'
            task.options = [
                file: new File('test.lua'),
                scripts: 'some/dir/with a space'
            ]

        then: '--file and --scripts are present in arguments'
            def path = new File('some/dir/with a space')
            task.arguments == ['vs2019', '--file=test.lua', "--scripts=$path"]

        when: 'remove scripts'
            task.options = [
                scripts: null
            ]

        then: '--scripts is removed from arguments'
            task.arguments == ['vs2019', '--file=test.lua']
    }

    def 'setting multiple options'() {
        when: 'set options via a map'
            task.options = [
                file: 'some script.lua',
                fatal: true,
                os: 'windows'
            ]

        and: 'set via a method'
            task.setOption('verbose', true)
            task.setOption('cc', 'gcc')

        then:
            task.arguments == ['vs2019', '--file=some script.lua', '--fatal', '--os=windows', '--verbose', '--cc=gcc']
    }

    def 'setting custom options'() {
        when: 'set options via a map'
            task.options = [
                foo: 'lorem',
                bar: true
            ]

        and: 'set via a method'
            task.setOption('fizz', true)
            task.setOption('buzz', '5')

        then:
            task.arguments == ['vs2019', '--foo=lorem', '--bar', '--fizz', '--buzz=5']
    }

    def 'platform specific executable on Linux'() {
        given: 'running on Linux'
            def origOsName = System.getProperty('os.name')
            System.setProperty('os.name', 'Linux')

        when: 'executable paths are changed'
            task.linuxPremakeExecutable = '/bin/premake5'
            task.windowsPremakeExecutable = 'C:\\Apps\\premake5.exe'
            task.macOSPremakeExecutable = '/Applications/premake5'

        then: 'Linux path is applied'
            task.premakeExecutable == new File('/bin/premake5')

        cleanup:
            System.setProperty('os.name', origOsName)
    }

    def 'platform specific executable on Windows'() {
        given: 'running on Windows'
            def origOsName = System.getProperty('os.name')
            System.setProperty('os.name', 'Windows')

        when: 'executable paths are changed'
            task.linuxPremakeExecutable = '/bin/premake5'
            task.windowsPremakeExecutable = 'C:\\Apps\\premake5.exe'
            task.macOSPremakeExecutable = '/Applications/premake5'

        then: 'Windows path is applied'
            task.premakeExecutable == new File('C:\\Apps\\premake5.exe')

        cleanup:
            System.setProperty('os.name', origOsName)
    }

    def 'platform specific executable on macOS'() {
        given: 'running on macOS'
            def origOsName = System.getProperty('os.name')
            System.setProperty('os.name', 'Mac OS X')

        when: 'executable paths are changed'
            task.linuxPremakeExecutable = '/bin/premake5'
            task.windowsPremakeExecutable = 'C:\\Apps\\premake5.exe'
            task.macOSPremakeExecutable = '/Applications/premake5'

        then: 'macOS path is applied'
            task.premakeExecutable == new File('/Applications/premake5')

        cleanup:
            System.setProperty('os.name', origOsName)
    }
}

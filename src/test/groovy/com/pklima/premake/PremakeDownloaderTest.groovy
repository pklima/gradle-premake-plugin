package com.gitlab.pklima.premake

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification
import java.nio.file.Files

import com.gitlab.pklima.premake.PremakeDownloader
import static com.gitlab.pklima.premake.PremakePlugin.LATEST_PREMAKE_VERSION

public class PremakeDownloaderTest extends Specification {
    def project = ProjectBuilder.builder().build()

    File outputFile
    PremakeDownloader downloader

    def setup() {
        Files.createDirectories(project.buildDir.toPath())
        outputFile = new File(project.buildDir, PremakePlugin.premakeExecutableName)
        downloader = new PremakeDownloader()
    }

    def 'download latest'() {
        given:
            downloader.version = ''

        when: 'we download premake'
            downloader.download(outputFile)

        then: 'the file is there'
            outputFile.exists() && outputFile.canExecute()

        when: 'we run the file'
            def result = [outputFile, '--version'].execute()
            result.waitFor()

        then: 'it runs and exits clearly'
            result.exitValue() == 0

        and: 'the version is the one we asked for'
            result.text.startsWith "premake5 (Premake Build Script Generator) $PremakePlugin.LATEST_PREMAKE_VERSION"
    }

    def 'download 5.0.0-alpha15'() {
        given:
            downloader.version = '5.0.0-alpha15'

        when: 'we download premake'
            downloader.download(outputFile)

        then: 'the file is there'
            outputFile.exists() && outputFile.canExecute()

        when: 'we run the file'
            def result = [outputFile, '--version'].execute()
            result.waitFor()

        then: 'it runs and exits clearly'
            result.exitValue() == 0

        and: 'the version is the one we asked for'
            result.text.startsWith 'premake5 (Premake Build Script Generator) 5.0.0-alpha15'
    }

    def 'download 5.0.0.alpha4'() {
        given:
            downloader.version = '5.0.0.alpha4'

        when: 'we download premake'
            downloader.download(outputFile)

        then: 'the file is there'
            outputFile.exists() && outputFile.canExecute()

        when: 'we run the file'
            def result = [outputFile, '--version'].execute()
            result.waitFor()

        then: 'it runs and exits clearly'
            result.exitValue() == 0

        and: 'the version is the one we asked for'
            result.text.startsWith 'premake5 (Premake Build Script Generator) 5.0.0.alpha4'
    }

    def 'invalid version'() {
        given: 'a non-existent version'
            downloader.version = '5.0.0-omega1'
        
        when: 'we try to download it'
            downloader.download(outputFile)

        then: 'it throws an exception'
            PremakeException e = thrown()
            e.message.startsWith 'Failed to download version \'5.0.0-omega1\''
    }
}

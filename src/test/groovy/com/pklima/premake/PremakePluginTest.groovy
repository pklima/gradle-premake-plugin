package com.gitlab.pklima.premake

import org.gradle.testfixtures.ProjectBuilder
import org.gradle.api.Project
import spock.lang.Specification

public class PremakePluginTest extends Specification {
    def 'plugin registers task'() {
        given:
            def project = ProjectBuilder.builder().build()

        when:
            project.plugins.apply('com.gitlab.pklima.premake')

        then:
            project.tasks.findByName('premake') != null
    }
}

package com.gitlab.pklima.premake

import groovy.transform.CompileStatic
import org.gradle.api.GradleException
import static com.gitlab.pklima.premake.PremakePlugin.PROJECT_URL
import static com.gitlab.pklima.premake.PremakePlugin.PLUGIN_NAME

/**
 * Any exception thrown directly by the premake plugin.
 */
@CompileStatic
class PremakeException extends GradleException {
    /**
     * Creates a premake plugin exception.
     *
     * @param message Exception message
     * @param meaCulpa Whether the exception is possibly caused by a bug in our code.
     *                 true if it is, false if we know for sure it's the user's fault.
     */
    PremakeException(String message, Boolean meaCulpa = false) {
        super(meaCulpa ? createMeaCulpaMessage(message) : message)
    }

    private static String createMeaCulpaMessage(String message) {
        return """$message

Please check that you are using the latest version of $PLUGIN_NAME.
If you are, please report this issue at $PROJECT_URL and include the full exception details."""
    }
}

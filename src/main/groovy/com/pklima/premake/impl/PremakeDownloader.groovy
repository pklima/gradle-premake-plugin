package com.gitlab.pklima.premake

import com.gitlab.pklima.premake.PremakeException
import com.gitlab.pklima.premake.PremakePlugin
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import groovyx.net.http.optional.Download
import java.nio.file.Files
import org.apache.commons.compress.archivers.ArchiveEntry
import org.apache.commons.compress.archivers.ArchiveStreamFactory
import org.apache.commons.compress.compressors.CompressorStreamFactory
import org.apache.commons.compress.utils.IOUtils
import org.gradle.api.logging.Logger
import static com.gitlab.pklima.premake.PremakePlugin.LATEST_PREMAKE_VERSION
import static groovyx.net.http.HttpBuilder.configure

/**
 * Downloads premake executables from the premake GitLab page (https://github.com/premake/premake-core/releases).
 */
@CompileStatic
class PremakeDownloader {
    /// Premake version to download.
    /// When empty, the latest version is downloaded.
    String version = PremakePlugin.LATEST_PREMAKE_VERSION

    /**
     * Creates the downloader.
     */
    PremakeDownloader() {}

    /**
     * Downloads the latest premake executable of the selected version.
     *
     * @param File to write the downloaded executable to.
     */
    void download(File outputExe) {
        def downloadVersion = findVersion()
        def url = getDownloadLink(downloadVersion)
        def downloadedFile = Files.createTempFile(null, getArchiveFileName(downloadVersion)).toFile()

        PremakePlugin.logger.debug "Downloading $url to $downloadedFile"

        try {
            configure {
                request.uri = url
            }.get {
                Download.toFile(delegate, downloadedFile)
            }
        } catch (Exception e) {
            def message = "Failed to download version '$downloadVersion' from '$url': $e.message"
            if (version) {
                message += '\nPlease make sure the provided version is valid and that you can access GitHub.'
            } else {
                message += '\nPlease check that you can access GitHub.'
            }
            message += ' If that\'s not the issue then:'
            throw new PremakeException(message, true)
        }

        def exeName = PremakePlugin.premakeExecutableName
        PremakePlugin.logger.debug "Extracting $exeName from $downloadedFile"

        if (extract(downloadedFile, exeName, outputExe)) {
            PremakePlugin.logger.debug "$outputExe successfully extracted"
        } else {
            throw new PremakeException("Downloaded zip file for version '$downloadVersion' from '$url' does not contain '$exeName'", true)
        }
    }

    /**
     * Extracts the given file out of the input archive.
     *
     * @param input Input archive.
     * @param fileName Name of the file to extract.
     * @param output File to extract the compressed file contents into.
     * @return true if the file was successfully extracted, false if it was not found.
     */
    private Boolean extract(File input, String fileName, File output) {
        try {
            def fileStream = input.newInputStream()

            // Check if we need to decrompress the file first (needed for gzip)
            BufferedInputStream compressorStream = null
            try {
                compressorStream = new BufferedInputStream(new CompressorStreamFactory().createCompressorInputStream(fileStream))
            } catch (CompressorException) {
                // Decompression is not necessary (e.g. zip), we can extract the archive directly.
                compressorStream = fileStream
            }

            def archiveStream = new ArchiveStreamFactory().createArchiveInputStream(compressorStream)

            // Iterate over each contained file until we find the premake executable.
            ArchiveEntry entry
            while (entry = archiveStream.getNextEntry()) {
                if (!entry.isDirectory() && entry.name == fileName) {
                    // Make sure the parent directory exists.
                    output.getParentFile().mkdirs()

                    // Copy out the extracted file.
                    def outputStream = output.newOutputStream()
                    IOUtils.copy(archiveStream, outputStream)
                    outputStream.close()

                    // Set executable permissions (important on Linux).
                    output.setExecutable(true)

                    return true
                }
            }
        } catch (Exception e) {
            throw new PremakeException("Failed to process downloaded archive '$input': $e.message", true)
        }

        return false
    }

    /**
     * Returns the achive file name for the given premake version.
     *
     * @param version Premake version to get the name for.
     * @return Name of the archive for the given version and the current OS.
     */
    private static String getArchiveFileName(String version) {
        switch (PremakePlugin.platform) {
            case Platform.LINUX:   return "premake-$version-linux.tar.gz"
            case Platform.WINDOWS: return "premake-$version-windows.zip"
            case Platform.MACOSX:  return "premake-$version-macosx.tar.gz"
            default: new PremakeException("Unexpected platform '$PremakePlugin.platform'", true)
        }
    }

    /**
     * Returns the download link for the given premake version.
     *
     * @param version Premake version to download.
     * @return Download link for the given version and the current OS.
     */
    private static String getDownloadLink(String version) {
        return "https://github.com/premake/premake-core/releases/download/v$version/${getArchiveFileName(version)}"
    }

    /**
     * Finds the premake release version matching #version.
     *
     * @return Name of the newest matching version.
     */
    @CompileDynamic
    String findVersion() {
        // If a version is set, use it.
        if (version) {
            return version
        }

        // Fetch all versions JSON objects and grab just the release (tag) name from them.
        def versions = fetchReleasesJson().collect { it.tag_name }
        if (!versions) {
            throw new PremakeException('Returned list of premake releases is empty', true)
        }

        // All versions we care about should start with 'v'.
        versions = versions.findAll { it.length() > 2 && it.startsWith('v') }
        if (!versions) {
            throw new PremakeException('Failed to find any release starting with a \'v\'', true)
        }

        // GitHub returns the releases in chronological order, newest ones first.
        // So use the first one, with the 'v' prefix stripped.
        def latestVersion = versions[0].substring(1)

        // XXX: Version 5.0.0-beta2 for Linux is messed up, we have to use the older version...
        //      (https://github.com/premake/premake-core/issues/1966)
        if (PremakePlugin.platform == Platform.LINUX && latestVersion == '5.0.0-beta2') {
            latestVersion = PremakePlugin.LATEST_PREMAKE_VERSION
        }

        return latestVersion
    }

    /**
     * Fetches a list of premake releases using the GitLab API.
     *
     * @return JSON of all releases
     */
    @CompileDynamic
    def fetchReleasesJson() {
        def url = 'https://api.github.com/repos/premake/premake-core/releases?per_page=100'
        try {
            return configure {
                request.uri = url
                request.accept = 'application/vnd.github.v3+json'
            }.get {
                response.parser('application/json')
            }
        } catch (Exception e) {
            throw new PremakeException("""Failed to fetch premake releases from GitHub using '$url': $e.message
Please check that you can access GitHub. If that's not the issue then:
""", true)
        }
    }
}

package com.gitlab.pklima.premake

import com.gitlab.pklima.premake.PremakeException
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging
import org.gradle.api.Plugin
import org.gradle.api.Project

/// Operating system platform.
public enum Platform {
    /// Linux or Unix (except macOS)
    LINUX,
    /// Microsoft Windows
    WINDOWS,
    /// Apple macOS
    MACOSX
}

@CompileStatic
public class PremakePlugin implements Plugin<Project> {
    public void apply(Project project) {
        project.apply(plugin: 'base')

        // Create the premake task.
        project.task(
            'premake',
            type: PremakeTask,
            description: 'Executes premake with the specified options.',
            group: 'build'
        )
    }

    /**
     * Returns the operating system platform.
     *
     * @return Detected OS platform.
     */
    static Platform getPlatform() {
        // Detection is pretty dodgy but org.apache.commons.lang3.SystemUtils does the same thing
        // so it seems to work well enough.
        def osName = System.getProperty('os.name')

        if (osName.startsWith('Linux') || osName.startsWith('LINUX')) {
            return Platform.LINUX
        } else if (osName.startsWith('Windows')) {
            return Platform.WINDOWS
        } else if (osName.startsWith('Mac OS X')) {
            return Platform.MACOSX
        } else {
            throw new PremakeException(
"""Unsupported operating system '$osName'.
Premake binaries are only available for Linux, Windows and macOS, so only these OSs are supported.
If you are using one of these OSs then then you are getting this message in error.""", true)
        }
    }

    /**
     * Returns the platform-appropriate name of the premake executable.
     *
     * @return Premake executable name for the current platform.
     */
    public static String getPremakeExecutableName() {
        return platform == Platform.WINDOWS ? 'premake5.exe' : 'premake5'
    }

    /**
     * Returns the logger instance for use by this plugin.
     *
     * @return Plugin's logger.
     */
    public static Logger getLogger() {
        return Logging.getLogger(PremakePlugin)
    }

    /// Name of the plugin.
    public static String PLUGIN_NAME = 'gradle-premake-plugin'

    /// URL where the project is hosted.
    public static String PROJECT_URL = 'https://gitlab.com/pklima/gradle-premake-plugin'

    /// Latest premake version known to us.
    public static String LATEST_PREMAKE_VERSION =
        // XXX: Version 5.0.0-beta2 for Linux is messed up
        //      (https://github.com/premake/premake-core/issues/1966)
        platform == Platform.LINUX ? '5.0.0-beta1' : '5.0.0-beta2'
}

package com.gitlab.pklima.premake

import com.gitlab.pklima.premake.PremakeDownloader
import com.gitlab.pklima.premake.PremakeException
import com.gitlab.pklima.premake.PremakePlugin
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction

@CompileStatic
class PremakeTask extends DefaultTask {
    /// Premake action to execute.
    @Input
    String action = ''

    /// Path to the premake5 executable.
    /// Defaults to `premake5` on Linux/macOS and `premake5.exe` on Windows,
    /// therfore looks for the executable on path.
    def premakeExecutable

    /// Directory to execute premake within.
    /// Defaults to `projectDir`.
    @InputDirectory
    File directory = project.projectDir

    /// Whether the premake executable should be downloaded if not present.
    @Input
    Boolean enableDownload = false

    @Internal
    /// Path to download the premake executable as, if downloading is enabled.
    File downloadedPremakeExecutable = new File(project.buildDir, PremakePlugin.premakeExecutableName)

    /**
     * Sets the Linux-specific executable path.
     *
     * If called on Linux, updates premakeExecutable to the given path.
     * If called on any other platform, nothing happens.
     *
     * @param premakeExecutable Path to the Linux premake executable.
     * @since 1.0.0
     */
    void setLinuxPremakeExecutable(def premakeExecutable) {
        if (PremakePlugin.getPlatform() == Platform.LINUX) {
            this.premakeExecutable = premakeExecutable
        } else {
            PremakePlugin.logger.debug "setLinuxPremakeExecutable ignored because platform (${PremakePlugin.getPlatform()}) is not Linux"
        }
    }

    /**
     * Sets the Windows-specific executable path.
     *
     * If called on Windows, updates premakeExecutable to the given path.
     * If called on any other platform, nothing happens.
     *
     * @param premakeExecutable Path to the Windows premake executable.
     * @since 1.0.0
     */
    void setWindowsPremakeExecutable(def premakeExecutable) {
        if (PremakePlugin.getPlatform() == Platform.WINDOWS) {
            this.premakeExecutable = premakeExecutable
        } else {
            PremakePlugin.logger.debug "setWindowsPremakeExecutable ignored because platform (${PremakePlugin.getPlatform()}) is not Windows"
        }
    }

    /**
     * Sets the macOS-specific executable path.
     *
     * If called on macOS, updates premakeExecutable to the given path.
     * If called on any other platform, nothing happens.
     *
     * @param premakeExecutable Path to the macOS premake executable.
     * @since 1.0.0
     */
    void setMacOSPremakeExecutable(def premakeExecutable) {
        if (PremakePlugin.getPlatform() == Platform.MACOSX) {
            this.premakeExecutable = premakeExecutable
        } else {
            PremakePlugin.logger.debug "setMacOSPremakeExecutable ignored because platform (${PremakePlugin.getPlatform()}) is not macOS"
        }
    }

    /**
     * Sets multiple premake options using a map.
     *
     * @param values Map of premake options to set.
     */
    @CompileDynamic
    void setOptions(def values) {
        values.each {
            switch (it.value) {
                case File:
                    setOption(it.key.toLowerCase(), it.value as File)
                    break
                case Boolean:
                    setOption(it.key.toLowerCase(), it.value as Boolean)
                    break
                case { it == null }:
                    setOption(it.key.toLowerCase(), (String)null)
                    break
                default:
                    setOption(it.key.toLowerCase(), it.value.toString())
                    break
            }
        }
    }

    /**
     * Sets the value of a string option.
     *
     * @param name Name of the option.
     * @param value Option value.
     */
    void setOption(String name, String value) {
        if (isFileOption(name)) {
            // If this is one of the options taking a file, forward it to the other method.
            setOption(name, value ? new File(value) : null)
        } else {
            def nameLow = name.toLowerCase()

            if (value == null) {
                // If the value is null, remove the option.
                options.remove(nameLow)
            } else {
                // Set the option value.
                options[nameLow] = value
            }
        }
    }

    /**
     * Sets whether an option is enabled or not.
     *
     * @param name Name of the option.
     * @param enable True to enable the option, false to disable it.
     */
    void setOption(String name, Boolean enable) {
        if (isFileOption(name)) {
            throw new PremakeException("Option '$name' may not be of type Boolean")
        }

        if (enable) {
            setOption(name, '')
        } else {
            setOption(name, (String)null)
        }
    }

    /**
     * Sets the value of a file path option.
     *
     * @param name Name of the option.
     * @param path Path to the file.
     */
    void setOption(String name, File path) {
        def nameLow = name.toLowerCase()

        switch(nameLow) {
            case 'file':
                file = path
            break
            case 'scripts':
                scripts = path
            break
            case 'systemscript':
                systemscript = path
            break
            default:
                setOption(nameLow, path.toString())
            break
        }
    }

    /**
     * Returns a map of the set options.
     *
     * @return Map of set premake options.
     */
    @Input
    def getOptions() {
        def opts = [:]
        if (file) {
            opts[file] = file
        }
        if (scripts) {
            opts[scripts] = scripts
        }
        if (systemscript) {
            opts[systemscript] = systemscript
        }

        return opts + options
    }

    /**
     * Returns the command line arguments which will be given to premake.
     *
     * @return Arguments for the premake process.
     */
    @Internal
    public List<String> getArguments() {
        def args = [action]

        if (file) {
            args << "--file=$file".toString()
        }
        if (scripts) {
            args << "--scripts=$scripts".toString()
        }
        if (systemscript) {
            args << "--systemscript=$systemscript".toString()
        }

        args += options.collect {
            if (!it.value) {
                // Option with no value.
                return "--$it.key".toString()
            } else {
                // Option with a value.
                return "--$it.key=$it.value".toString()
            }
        }

        return args
    }

    /**
     * Sets the premake version to download, if the executable is missing and download is enabled.
     * When set to an empty string (default), the latest version is downloaded.
     *
     * @param version Premake version to download.
     */
    @CompileDynamic
    void setDownloadVersion(def version) {
        downloader.version = version
    }

    /**
     * Returns the premake version download.
     * @see setDownloadVersion()
     *
     * @return Premake version to download.
     */
    @Input
    String getDownloadVersion() {
        return downloader.version
    }

    /**
     * @see directory
     */
    void setDirectory(def path) {
        directory = makeFile(path)
    }

    /**
     * @see premakeExecutable
     */
    @Internal
    @CompileDynamic
    File getPremakeExecutable() {
        // If premakeExecutable was not set, return the default.
        if (!premakeExecutable) {
            return new File(PremakePlugin.premakeExecutableName)
        }

        // Make sure we return a File instance
        return makeFile(premakeExecutable)
    }

    /**
     * Do not use, use getPremakeExecutable() instead!
     * Workaround for @InputFile requiring the file to exist, even if marked @Internal.
     */
    @InputFile
    @Optional
    File getPremakeExecutableInternal() {
        def executable = getPremakeExecutable()
        return executable.exists() ? executable : null
    }

    /**
     * @see downloadedPremakeExecutable
     */
    void setDownloadedPremakeExecutable(def path) {
        downloadedPremakeExecutable = makeFile(path)
    }

    @TaskAction
    @CompileDynamic
    void call() {
        def premakeExe = getPremakeExecutable()
        if (!premakeExe.exists()) {
            if (!enableDownload) {
                throw new PremakeException("'$premakeExe' does not exist. If you wish it to be automatically downloaded, set enableDownload = true.")
            }

            premakeExe = downloadedPremakeExecutable

            PremakePlugin.logger.info "Premake executable not found, downloading version '${downloadVersion ? downloadVersion : '<latest>'}' to '$premakeExe'..."
            downloader.download(premakeExe)
        }

        project.exec {
            executable = premakeExe
            workingDir = getDirectory()
            args = getArguments()
        }
    }

    @CompileDynamic
    private static File makeFile(def value) {
        if (value instanceof File) {
            return value
        } else {
            return new File(value)
        }
    }

    private String writeValue(File value) {
        return writeValue(value.toString())
    }

    private String writeValue(String value) {
        return value =~ /\s+/ ? "\"$value\"" : value
    }

    private Boolean isFileOption(String name) {
        return name.equalsIgnoreCase('file') ||
            name.equalsIgnoreCase('scripts') ||
            name.equalsIgnoreCase('systemscript')
    }

    /// Premake "file" option.
    private File file

    /// Premake "scripts" option.
    private File scripts

    /// Premake "systemscript" option.
    private File systemscript

    /// Remaining premake options.
    private final Map<String, String> options = [:]

    /// Used to download premake, if missing.
    private PremakeDownloader downloader = new PremakeDownloader()
}

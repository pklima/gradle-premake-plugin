package com.gitlab.pklima.premake

import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

public class PremakeTaskDownloadFunctionalTest extends Specification {
    @Rule TemporaryFolder testProjectDir = new TemporaryFolder()
    File buildScript = null
    File buildDir = null

    def setup() {
        buildScript = testProjectDir.newFile('build.gradle')
        buildScript << '''
            plugins {
                id 'com.gitlab.pklima.premake'
            }
        '''

        buildDir = testProjectDir.newFolder('build')
    }

    def 'download latest premake'() {
        given:
        buildScript << '''
            premake {
                options = [
                    version: true
                ]
                enableDownload = true
            }
        '''

        def runner = GradleRunner.create()
        runner.forwardOutput()
        runner.withPluginClasspath()
        runner.withArguments(['premake', '--info', '--stacktrace'])
        runner.withProjectDir(testProjectDir.root)
        def result = runner.build()

        expect:
        result.output.contains('premake5 (Premake Build Script Generator)')
    }

    def 'download premake 5.0.0-alpha14'() {
        given:
        buildScript << '''
            premake {
                options = [
                    version: true
                ]
                enableDownload = true
                downloadVersion = '5.0.0-alpha14'
            }
        '''

        def runner = GradleRunner.create()
        runner.forwardOutput()
        runner.withPluginClasspath()
        runner.withArguments(['premake', '--info', '--stacktrace'])
        runner.withProjectDir(testProjectDir.root)
        def result = runner.build()

        expect:
        result.output.contains('premake5 (Premake Build Script Generator) 5.0.0-alpha14')
    }
}

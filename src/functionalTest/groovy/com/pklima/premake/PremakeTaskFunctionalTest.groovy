package com.gitlab.pklima.premake

import org.apache.tools.ant.taskdefs.condition.Os
import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

public class PremakeTaskFunctionalTest extends Specification {
    @Rule
    TemporaryFolder testProjectDir = new TemporaryFolder()
    File buildScript = null

    String enquote(String param) {
        if (Os.isFamily(Os.FAMILY_WINDOWS)) {
            // On Windows quotes around the parameter are kept
            return "\"$param\""
        } else {
            // In Bash they are.
            return param
        }
    }

    def setup() {
        testProjectDir.newFile('settings.gradle') << ''

        buildScript = testProjectDir.newFile('build.gradle') << '''
            plugins {
                id('com.gitlab.pklima.premake')
            }
        '''

        // Write the fake premake
        def buildDir = testProjectDir.newFolder('build')

        // On Windows write a batch file.
        new File(buildDir, 'premake5.bat') << '''
            @echo off
            echo PARAMS:[%*]
        '''

        // On *nix write a bash script and make it executable.
        def bashScript = new File(buildDir, 'premake5') << '''
            #!/bin/bash
            echo PARAMS:[$@]
        '''
        bashScript.setExecutable(true)

        // Point the task to our fake premake
        buildScript << '''
            premake {
                linuxPremakeExecutable = "$buildDir/premake5"
                macOSPremakeExecutable = "$buildDir/premake5"
                windowsPremakeExecutable = "$buildDir\\\\premake5.bat"
            }
        '''
    }

    def 'only action'() {
        given: 'only action is given'
            buildScript << '''
                premake {
                    action = 'vs2017'
                }
            '''
            def runner = GradleRunner.create()
            runner.forwardOutput()
            runner.withPluginClasspath()
            runner.withArguments('premake')
            runner.withProjectDir(testProjectDir.root)

        when: 'we run the build'
            def result = runner.build()

        then: 'only action is passed into premake'
            result.output.contains('PARAMS:[vs2017]')
    }

    def 'many options'() {
        given: 'action and options are given'
            buildScript << '''
                premake {
                    action = 'vs2019'

                    options = [
                        file:    'some script.lua',
                        scripts: 'some/dir/with a space',
                        fatal:   true,
                        os:      'windows',
                        verbose: true,
                        cc:      'gcc',
                        custom:  'foo'
                    ]
                }
            '''

            def runner = GradleRunner.create()
            runner.forwardOutput()
            runner.withPluginClasspath()
            runner.withArguments('premake')
            runner.withProjectDir(testProjectDir.root)

        when: 'we run the build'
            def result = runner.build()

        then: 'action and all options are passed into premake'
            def path = new File('some/dir/with a space')
            result.output.contains("PARAMS:[vs2019 ${enquote('--file=some script.lua')} ${enquote("--scripts=$path")} --fatal --os=windows --verbose --cc=gcc --custom=foo]")
    }
}

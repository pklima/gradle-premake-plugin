# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2023-11-24

### Added

- Added `downloadedPremakeExecutable` task property.
- Added `linuxPremakeExecutable`, `windowsPremakeExecutable`, and `macOSPremakeExecutable` as platform-specific
  alternatives for the `premakeExecutable` task property.
- Run tests for Gradle versions 5.6.4, 6.9.4, 7.6.3, and 8.4.

### Changed

- **Breaking:** Default `premakeExecutable` path when downloading is now just the executable name,
  allowing it to be looked up on path.
- Updated latest known premake version to 5.0.0-beta2.
    - Note that because of a [bug in premake](https://github.com/premake/premake-core/issues/1966),
      on Linux the latest version is is held back to 5.0.0-beta1.

### Fixed

- **Breaking:** Setting `premakeExecutable` path no longer makes it relative to the project directory.
- Build script was not compatible with Gradle 7.

## [0.0.1] - 2021-02-21

### Added

- Initial release.

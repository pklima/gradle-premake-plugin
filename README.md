Gradle Premake Plugin
=====================

A simple Gradle plugin which adds a **premake** task for executing [premake5](https://github.com/premake/premake-core/wiki).

You can either supply the `premake5` executable yourself, or enable it to be downloaded from the Internet (specifically from the [GitHub releases page](https://github.com/premake/premake-core/releases)) .

Please note that this plugin will only generate the project files, it will not handle the compilation for you.

Applying the Plugin
-------------------

Plugins DSL:
```gradle
plugins {
    id 'com.gitlab.pklima.premake' version '1.0.0'
}
```

Legacy:
```gradle
buildscript {
    repositories {
        maven {
            url 'https://plugins.gradle.org/m2/'
        }
    }
    dependencies {
        classpath 'com.gitlab.pklima:gradle-premake-plugin:0.0.1'
    }
}

apply plugin: 'com.gitlab.pklima.premake'
```

Task Configuration
------------------
```gradle
premake {
    // Premake action to execute.
    // Default: None, so you must always set this!
    action = 'vs2019'

    // Set the path to the premake5 executable.
    // Default: `premake5` on Linux and macOS,
    //          `premake5.exe` on Windows
    premakeExecutable = 'premake5'

    // As an alternative to the above, you can also specify different paths
    // for the different platforms at the same time, with only the one
    // the build is running on being applied.
    linuxPremakeExecutable = '/bin/premake5'
    windowsPremakeExecutable = 'C:\\Apps\\premake5.exe'
    macOSPremakeExecutable = '/Applications/premake5'

    // Set the directory which premake5 will be executed in.
    // This is relevant for any relative paths you may be using inside your the premake lua script.
    // Default: $projectDir
    directory = "$projectDir"

    // Whether the premake5 executable should be downloaded if `premakeExecutable` doesn't exist.
    // Note that the file will be downloaded to a path specified by `downloadedPremakeExecutable`.
    // Default: false
    enableDownload = true

    // Directory to download the premake executable into, if downloading is enabled.
    // Default: `$buildDir/premake5` on Linux and macOS,
    //          `$buildDir/premake5.exe` on Windows
    downloadedPremakeExecutable = "${Files.createTempDirectory('premake')}/premake5"

    // If premake5 is to be downloaded, the exact version to download.
    // This can be set to an empty string to automatically download the latest version,
    // however this is not recommended because:
    //   1. You can't be sure that your script is still compatible with the new version.
    //   2. Latest version is determined using the GitHub API without authentication.
    //      It is therefore subject to a strict per-IP address rate limit:
    //      https://docs.github.com/en/rest/overview/resources-in-the-rest-api#rate-limiting
    //
    // Default: 5.0.0-beta2 (will be updated with each release to the latest version)
    downloadVersion = '5.0.0-alpha15'

    // Set the premake5 options. This includes both the default premake ones,
    // as well as any your premake script may be adding via `newoption()`.
    // To get the full list you can execute with the `help` option enabled.
    // The list below are just examples.
    // Defaults: premake-determined.
    options = [
        // Set `--fatal`.
        fatal: true,

        // Do NOT set `--version`.
        // Note that this is equivalent to just leaving the option out.
        version: false,

        // Set `--file`.
        file: 'myPremakeScript.lua',

        // Set a custom `--animal` option.
        animal: 'kangaroo'
    ]
}
```


Examples
--------
Minimal example which makes the following assumptions:
- Premake executable is already present in `build/premake5`
- The premake script file is called `premake.lua` and is located in the project root directory.

```gradle
premake {
    action = 'vs2019'
}
```

Minimal example with download enabled:
```gradle
premake {
    action = 'vs2019'
    enableDownload = true
}
```

List available premake options:
```gradle
premake {
    help = true
}
```

Complex example:
```gradle
premake {
    premakeExecutable = 'premake5' // Use system executable
    action = 'gmake2'
    directory = 'premake'
    options = [
        file: 'module.lua',
        os: 'linux',
        cc: 'gcc',
        fatal: true
    ]
}
```

Compatibility
-------------
The plugin is guaranteed to be compatible with the following:


| Gradle | Java |
| ------ | ---- |
| 8.4    |  21  |
| 7.6.3  |  17  |
| 6.9.4  |  11  |
| 5.6.4  |  11  |
| 4.10.3 |   8  |
